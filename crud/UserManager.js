// function UserManager() {
//     this.users = {};
// }
//
// UserManager.prototype.addUser= function(user){
//     let id = user.id;
//     if(!this.users[id]) {
//         this.users[id] = user;
//     }
// };
//
// UserManager.prototype.updateUser= function(user){
//     let id = user.id;
//     let oldUser = this.users[id];
//     if(oldUser) {
//         this.users[id] = user;
//     }
// };
//
// UserManager.prototype.deleteById= function(id){
//     delete this.users[id];
// };
//
// UserManager.prototype.getUsers= function(id){
//     return this.users;
// };
//
// UserManager.prototype.byId= function(id){
//     return this.users[id];
// };

function UserManager() {
    this.users = {};
}

UserManager.prototype.addUser= async function(user){
    // let user1 = {"id": 33, "firstName": "John", "lastName": "Silver", "age": 52};
    let response = await fetch('http://localhost:8080/create', {
        method: "POST",
        body: JSON.stringify(user),
        headers: {

            'Content-Type': 'application/json'
        }
    });

    let data = await response.json();
    console.log(data);
    for (let user of data['results']) {
        this.users[user.id] = user;
    }
};

UserManager.prototype.updateUser= async function(user){

    let response = await fetch('http://localhost:8080/update', {
        method: "POST",
        body: JSON.stringify(user),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    let data = await response.json();
    let id = user.id;
    // let oldUser = this.users[id];
    for (let user of data['results']) {
        this.users[user.id] = user;
    }
};

UserManager.prototype.deleteById = async function(id){
    let user = {id: id};
    let response = await fetch('http://localhost:8080/delete', {
        method: "POST",
        body: JSON.stringify(user),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    let data = await response.json();

    // let oldUser = this.users[id];
    this.users = {};

    for (let user of data['results']) {
        this.users[user.id] = user;
    }

    // delete this.users[id];
};

UserManager.prototype.getUsers= function(id){
    return this.users;
};

UserManager.prototype.byId= function(id){
    return this.users[id];
};