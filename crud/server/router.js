const fs = require("fs");
const express = require('express');
let connection = require('./mysqlDB');
const router = express.Router();

// router.get('/api', function (req, res) {
//
//     // let fileContent = fs.readFileSync("candle.txt", "utf8");
//     // res.send({fileContent});
//
//     connection.query('SELECT * FROM candles', function (error, results, fields) {
//         res.send({results});
//     });
//
// });

router.post('/create', function (req, res) {

    console.log(req.body);
    // let user = JSON.parse(req.body);
    let user = req.body;

    // let fileContent = fs.readFileSync("candle.txt", "utf8");
    // res.send({fileContent});

    let sql = "INSERT INTO users (id, first_name, last_name, age) VALUES ?";
    let values = [
        [
            user.id,
            user.firstName,
            user.lastName,
            user.age
        ]
    ];
    connection.query(sql, [values], function (err, result) {
        if (err) throw err;
        console.log("New user" + result.affectedRows);
    });

    connection.query('SELECT * FROM users', function (error, results, fields) {
        res.send({results});
    });

});


router.post('/update', function (req, res) {

    console.log(req.body);
    // let user = JSON.parse(req.body);
    let user = req.body;

    // let fileContent = fs.readFileSync("candle.txt", "utf8");
    // res.send({fileContent});

    let sql = "UPDATE users  SET ? WHERE ?";
    let values = [
        {
            first_name: user.firstName,
            last_name:user.lastName,
            age: user.age
        },
        {
            id: user.id
        }
    ];
    connection.query(sql, values, function (err, result) {
        if (err) throw err;
        console.log("New user" + result.affectedRows);
    });

    connection.query('SELECT * FROM users', function (error, results, fields) {
        console.log(results)
        res.send({results});
    });

});

router.post('/delete', function (req, res) {

    console.log(req.body);
    // let user = JSON.parse(req.body);
    let user = req.body;

    // let fileContent = fs.readFileSync("candle.txt", "utf8");
    // res.send({fileContent});

    let sql = "DELETE FROM users  WHERE ?";

    let values = [
        {
            id: user.id
        }
    ];

    connection.query(sql, values, function (err, result) {
        if (err) throw err;
        console.log("Number of records deleted: " + result.affectedRows);
    });

    connection.query('SELECT * FROM users', function (error, results, fields) {
        res.send({results});
    });

});

module.exports = router;