function Controller() {

    this.btnCreate = document.getElementById('create');
    this.btnRead = document.getElementById('read');
    this.btnUpdate = document.getElementById('update');
    this.btnDelete = document.getElementById('delete');
    this.btnAddStart = document.getElementById('addStart');
    this.btnAddMiddle = document.getElementById('addMiddle');
    this.btnAddEnd = document.getElementById('addEnd');
    this.btnSave = document.getElementById('save');
    this.btnRestore = document.getElementById('restore');

    this.inputJson = document.getElementById('inputJson');
    this.tbody = document.getElementById('tbody');

    this.array = [];

    this.manager = new UserManager();
}


Controller.prototype.parse = function (value) {
    return JSON.parse(value);
};

Controller.prototype.renderUsers = function () {
    let content = '';
    for (let id in this.manager.getUsers()){
        let user = this.manager.byId(id);
        content+=`<tr><td>${user.id}</td><td>${user.first_name}</td><td>${user.last_name}</td><td>${user.age}</td></tr>`
    }
    this.tbody.innerHTML = content;
};

Controller.prototype.init = function () {

    this.btnCreate.addEventListener('click', async (e) => {
        let user = this.parse(this.inputJson.value);
        // let user = {};
        await this.manager.addUser(user);
        this.renderUsers();
    });

    this.btnRead.addEventListener('click', (e) => {
        let id = this.inputJson.value;
        let user = this.manager.byId(id);
        if (user) {
            this.inputJson.value = `{"id":${user.id}, "firstName": "${user.first_name}", "lastName": "${user.last_name}", "age": "${user.age}"  }`;
        }
        this.renderUsers();
    });

    this.btnUpdate.addEventListener('click', async (e) => {
        let user = this.parse(this.inputJson.value);
        await this.manager.updateUser(user);
        this.renderUsers();
    });

    this.btnDelete.addEventListener('click', async (e) => {
        let id = this.inputJson.value;
        await this.manager.deleteById(id);
        this.renderUsers();
    })
};





